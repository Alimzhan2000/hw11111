//////////////////////////////////////////////////////////////////////
//
// Given is a producer-consumer szenario, where a producer reads in
// tweets from a mockstream and a consumer is processing the
// data. Your task is to change the code so that the producer as well
// as the consumer can run concurrently
//

package main

import (
	"fmt"
	"time"
)

func producer(stream Stream, tweets chan Tweet) {
	for {
		tweet, err := stream.Next()
		if err == ErrEOF {
			// to notify listening G to stop
			close(tweets)
			return
		}




		tweets <- *tweet
		//tweets = append(tweets, tweet)
	}
}

func consumer(tweets chan Tweet) {
	// channel listening
	// insteadd of just iterating
	for t := range tweets {
		if t.IsTalkingAboutGo() {
			fmt.Println(t.Username, "\ttweets about golang")
		} else {
			fmt.Println(t.Username, "\tdoes not tweet about golang")
		}
	}
}

func main() {
	start := time.Now()
	stream := GetMockStream()


	// unbuffered chan
	tweets := make(chan Tweet)


	// here i share one chan
	// Producer
	go producer(stream, tweets)

	// Consumer
	consumer(tweets)

	fmt.Printf("Process took %s\n", time.Since(start))
}